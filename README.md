# docker-spatialite

Dockerfile with SpatiaLite and Python installed.


## Release Notes

### 0.0.2

* Install with apt-get: python-dev, libxml2-dev, libxslt1-dev and zlib1g-dev
* Install with pip: setuptools, amqp and anyjson
* Add requirements file and install it.



### 0.0.1

* Python 2.7.6
* SpatiaLite - a Spatial extension for SQLite



## Example Commands ##

```shell
docker build -t local/spatialite .
```

```shell
docker run -it \
    --rm \
    --volume=[SOURCE_CODE]:/app \
    --workdir=/app \
    --entrypoint=/bin/bash local/spatialite
```
